#!/bin/bash

#
# simple set of steps to globaly test app environment and some crucital functions
#
#  !!! SHOULD WORK ONLY ON LOCAL DEV ENV !!!
#

# SET -e (exit on error) -x (trace and print all commands)
set -e

TEST_STACK_NAME="reverse_proxy_test"
WAIT_MAX_INTERATIONS=60
# WAIT_MAX_INTERATIONS=60

# set -ex
color_red='\033[1;31m'
color_yellow='\033[1;33m'
color_white='\033[1;37m'
color_white_bggreen='\033[1;37;42m'
color_not='\033[0m' # No Color

BIN_FIND=$(which find);
BIN_BASH=$(which bash);

# set my directory
MY_DIRPATH="`dirname \"$0\"`"              # relative
MY_DIRPATH="`( cd \"${MY_DIRPATH}\" && pwd )`"  # absolutized and normalized
if [ -z "${MY_DIRPATH}" ] ; then
  # error; for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  exit 1  # fail
fi

# set files to test
if [ -z "${1}" ]; then
    TESTS_FILES=$(${BIN_FIND} ${MY_DIRPATH} -type f -name '*.test');
else
    TESTS_FILES=${@};
fi

# ########################################
# run test containers via our init script
printf "\n\n${color_yellow}Deploying test swarm...${color_not}\n\n";
${BIN_BASH} -ex ${MY_DIRPATH}/../bin/docker.stack.deploy.bash ${TEST_STACK_NAME};

# sest exit cleanup function
clean_up () {
    ARG=$?

    # shutdown swarm
    if [ ${ARG} -ne 0 ]; then
        printf "\n\n${color_red}ERROR: Some test above ended-up with error no.${ARG}...${color_not}\n";
        printf "\n${color_red}Trying to shutdown test swarm...${color_not}\n\n";
        printf "${color_red}";
    else
        printf "\n\n${color_yellow}Trying to shutdown test swarm...${color_not}\n\n";
        printf "${color_not}";
    fi
    ${BIN_BASH} -ex ${MY_DIRPATH}/../bin/docker.stack.rm.bash ${TEST_STACK_NAME};

    # rm volumes
    local testvolumes=$(docker volume ls --filter name=${TEST_STACK_NAME} --format "{{.Name}}")
    if [ ! -z "$testvolumes" ]; then
        if [ ${ARG} -ne 0 ]; then
            printf "\n${color_red}Trying to rm test volumes...${color_not}\n\n";
            printf "${color_red}";
        else
            printf "\n\n${color_yellow}Trying to rm test volumes...${color_not}\n\n";
            printf "${color_not}";
        fi
        echo "removing volumes..."
        docker volume rm --force ${testvolumes};
    fi

    printf "${color_not}";
    exit $ARG
}
trap clean_up EXIT

# #################################################################
# wait for docker swarm services to be up and running or completed
printf "\n\n${color_white}Waiting for swarm services (wait max ${WAIT_MAX_INTERATIONS} iterations/seconds)...${color_not}\n\n";
testserviceslist=$(docker service ls --filter name=${TEST_STACK_NAME} --format "{{.Name}}")
if [ -z "$testserviceslist" ]; then
    echo 'No test services found!'
    exit 1;
fi
WAIT_MAX_INTERATIONS_COUNTER=0
while true; do

    WAIT_MAX_INTERATIONS_COUNTER=$(($WAIT_MAX_INTERATIONS_COUNTER+1))
    echo ${WAIT_MAX_INTERATIONS_COUNTER}
    if [ ${WAIT_MAX_INTERATIONS_COUNTER} -gt ${WAIT_MAX_INTERATIONS} ]; then
        echo "Some or all services didn't start! Exitting..."
        exit 1;
    fi

    isrunning=true
    for testservicecurrstate in $(docker service ps ${testserviceslist} --format "{{.Name}} {{.CurrentState}}" | awk '{print $1 "-->" $2}'); do
        echo ${testservicecurrstate}
        if [[ ${testservicecurrstate} != *"-->Running" && ${testservicecurrstate} != *"-->Complete" ]];then
            isrunning=false
        fi
    done
    if [ "${isrunning}" = true ] ; then
        echo "All services are running or complete now..."
        break
    fi

    sleep 1;
done

# ####################################
# look for tests in directory and run
for filepathtest in ${TESTS_FILES}; do
    printf "\n\n${color_yellow}Running test file...\n${color_white}${filepathtest}${color_not}\n\n";
    ${BIN_BASH} -ex ${filepathtest};
done

printf "\n\n${color_white_bggreen}All tests OK...${color_not}\n\n";
