
# Simple dockerized nginx proxy for local docker stacks

## @todo

    - simplify nginx vhosts configuration
<!-- - nothing todo for now (to be recorded here in readme) -->

## Production: HowTo

**The first time install:**  
$ docker -v  
$ docker-compose -v  
$ Copy following files from the repository git@bitbucket.org:xklid101/reverse.proxy.docker.git  
(The latest tagged files for production! For staging use any branch you need)  
    - .letsencrypt/\*
    - bin/\*  
    - configs/\*.sample.\*  
    - secrets/\*.sample.\*  
    - docker-compose.yml  
    - docker-compose.override.sample.yml  
*Try this command to download all files:*  
*(replace [ref] with valid refference - e.g. `dev` branch name, or `1.0.2` tag, ...)*
```
git archive --format tar --remote ssh://git@bitbucket.org/xklid101/reverse.proxy.docker.git \
[ref]: .letsencrypt bin configs secrets docker-compose* --output ./assets.tar && \
tar xf assets.tar && \
rm assets.tar
```
`for file in ./secrets/*.sample.*; do cp "$file" "${file/\.sample\./\.}"; done`  
    - edit secrets as needed   
`for file in ./configs/*.sample.*; do cp "$file" "${file/\.sample\./\.}"; done`  
    - edit configuration as needed   
`cp docker-compose.override.sample.yml docker-compose.override.yml`  
    - edit configuration as needed  
`./bin/docker.stack.deploy.bash`  

for letsencrypt & certbot scripts in ./bin usage:  
be a root or run  
`chmod -R 0777 ./.letsencrypt`  
and run following even you are a root  
`chmod 0777 ./.letsencrypt/webroot/.well-known/acme-challenge`

**Second time and later install/update:**  
Manually edit docker-compose\*.yml and run ./bin/docker.stack.deploy.bash to update services  

## Development: HowTo

$ git --version  
$ docker -v  
$ docker-compose -v  
$ composer --version  
$ git clone git@bitbucket.org:xklid101/reverse.proxy.docker.git reverse.proxy.docker && cd reverse.proxy.docker  
$ for file in ./secrets/\*.sample.\*; do cp "$file" "${file/\\.sample\\./\\.}"; done  
    - edit secrets as needed   
$ for file in ./configs/\*.sample.\*; do cp "$file" "${file/\\.sample\\./\\.}"; done  
    - edit configuration as needed   
$ cp docker-compose.override.sample.conf docker-compose.override.conf  
    - edit configuration as needed 
$ docker compose build  
$ ./tests/test.bash  
    - check git that no files has been changed after test! If so, something is wrong!
    (Or doublecheck what has been changed and why and mabye its ok)  
$ ./bin/docker.stack.deploy.bash

## Local Staging: HowTo

the same as production, except:  
- docker-compose-override.yml should use production version and! `:*-local-dev` tags for images
- use only self-signed certificates (do not use letsencrypt scripts because it will fail due to local env)
- run `./bin/docker.stack.deploy.bash reverse_proxy_staging` to distinguish staging env (volumes etc)
- run `./bin/docker.stack.rm.bash reverse_proxy_staging` to distinguish staging env (volumes etc)
- after stanging is removed, just delete staging volumes or just keep staging to continue next time

### Development And Production Hints

1) **Prod & Dev:** Docker creates nginx service and network named `reverse_proxy` with overlay docker driver. So all other services must connect to this network to be able to use this proxy!

2) **Prod & Dev:** If on localhost - have to add proxy_pass host from configs/nginx.vhosts.conf
into /etc/hosts to properly access local services  

3) **Prod & Dev:** Other local services should run on ports that are blocked by some firewall from outside world and be accessible only via this proxy.
Allowing ports 22, 80 and 443 on firewall should be enough (ipv4 and ipv6)

### Dockerfiles refs

[1.0.3 Dockerfiles](https://bitbucket.org/xklid101/reverse.proxy.docker/src/1.0.3/.docker)
[1.0.2 Dockerfiles](https://bitbucket.org/xklid101/reverse.proxy.docker/src/1.0.2/.docker)
[1.0.1 Dockerfiles](https://bitbucket.org/xklid101/reverse.proxy.docker/src/1.0.1/.docker)  
[1.0.0 Dockerfiles](https://bitbucket.org/xklid101/reverse.proxy.docker/src/1.0.0/.docker)  

