#!/bin/bash
STACK_NAME_PASS="reverse_proxy"

# EDIT MANUALLY TO GET FROM PRIVATE REPOS WITH REGISTRY AUTH CREDENTIALS OR NOT
# OPT_WITH_REGISTRY_AUTH="--with-registry-auth"
OPT_WITH_REGISTRY_AUTH=""

STACK_NAME=${1}
if [[ -z ${STACK_NAME} ]]; then
  echo "missing STACK_NAME argument!"
  echo "use like: './docker.stack.deploy.bash STACK_NAME'"
  exit 1
fi
if [[ ${STACK_NAME} != *"${STACK_NAME_PASS}"* ]]; then
  echo "\$STACK_NAME argument (${STACK_NAME}) passed to the script has to contain \"${STACK_NAME_PASS}\" string!"
  exit 1
fi

set -e

# get this script directory
DIR=$(cd `dirname $0` && pwd)
# go directory up from this ./bin directory
echo "cd ${DIR}/.."
cd ${DIR}/..


# define config and secrets versions variables by md5 sum of each (skip *.sample.*)
#   and export as a variables needed for deploy
for dirname in configs secrets;
do
    for file in ./${dirname}/*.*;
    do
        if [[ ${file} == *".sample."* ]]; then
            continue
        fi
        FILEBASE=$(basename ${file})
        VERSION_sub=$(md5sum ${file} | awk '{print $1}')
        # include stack name into checksum to prevent duplications
        VERSION=$(md5sum <<< "${VERSION_sub}_${STACK_NAME}" | awk '{print $1}')
        SUFFIX=${FILEBASE//\./_}
        echo "export \"CS_VERSION_${dirname}_${SUFFIX}=${VERSION}\""
        export "CS_VERSION_${dirname}_${SUFFIX}=${VERSION}"
    done
done


# run docker stack with use of docker-compose config preprocess
# docker-compose --log-level ERROR config
echo "docker-compose --log-level ERROR config | docker stack deploy -c - ${STACK_NAME}"
docker-compose --log-level ERROR config | docker stack deploy ${OPT_WITH_REGISTRY_AUTH} -c - ${STACK_NAME}
