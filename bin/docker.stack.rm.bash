#!/bin/bash
STACK_NAME_PASS="reverse_proxy"

STACK_NAME=${1}
if [[ -z ${STACK_NAME} ]]; then
  echo "missing STACK_NAME argument!"
  echo "use like: './docker.stack.deploy.bash STACK_NAME'"
  exit 1
fi
if [[ ${STACK_NAME} != *"${STACK_NAME_PASS}"* ]]; then
  echo "\$STACK_NAME argument (${STACK_NAME}) passed to the script has to contain \"${STACK_NAME_PASS}\" string!"
  exit 1
fi

# shutdown docker stack by name
docker stack rm ${STACK_NAME}
echo "stopping containers..."
docker stop $(docker ps -aq --filter name=${STACK_NAME})
echo "removing containers..."
docker rm $(docker ps -aq --filter name=${STACK_NAME})
