#!/bin/bash

# RENEW CERTIFICATES (usable for cronjob - once a 12 hours recommended)

#########################################################
# usable only in production/publicly accessible domains #

# get this script directory
DIR=$(cd `dirname $0` && pwd)

# PASS "live" argument not to perform --dry-run
OPT_DRY_RUN="--dry-run"
if [ "$1" == "live" ]; then
    OPT_DRY_RUN=""
fi

# run certbot with proper configuration options
docker run -it --rm \
-v ${DIR}/../.letsencrypt/etc/letsencrypt:/etc/letsencrypt \
-v ${DIR}/../.letsencrypt/var/lib/letsencrypt:/var/lib/letsencrypt \
-v ${DIR}/../.letsencrypt/webroot:/letsencrypt/webroot \
-v ${DIR}/../.letsencrypt/var/log/letsencrypt:/var/log/letsencrypt \
certbot/certbot \
renew \
${OPT_DRY_RUN} \
--webroot \
--webroot-path=/letsencrypt/webroot
