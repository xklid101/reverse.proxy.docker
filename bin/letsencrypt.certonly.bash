#!/bin/bash

# CREATE CERTIFICATES AND SAVE INTO "../.letsencrypt" directory

#########################################################
# usable only in production/publicly accessible domains #

# get this script directory
DIR=$(cd `dirname $0` && pwd)

# PASS "live" argument not to perform --dry-run
OPT_DRY_RUN="--dry-run"
if [ "$1" == "live" ]; then
    OPT_DRY_RUN=""
fi

# run certbot with proper configuration options
docker run -it --rm \
-v ${DIR}/../.letsencrypt/etc/letsencrypt:/etc/letsencrypt \
-v ${DIR}/../.letsencrypt/var/lib/letsencrypt:/var/lib/letsencrypt \
-v ${DIR}/../.letsencrypt/webroot:/letsencrypt/webroot \
-v ${DIR}/../.letsencrypt/var/log/letsencrypt:/var/log/letsencrypt \
certbot/certbot \
certonly \
${OPT_DRY_RUN} \
--webroot \
--webroot-path=/letsencrypt/webroot \
-d atelierkresbymalby.cz \
-d www.atelierkresbymalby.cz \
-d invoicing.otompotom.cz \
-d updateui.otompotom.cz \
-d adminer.otompotom.cz \
--email xklid101@gmail.com \
--agree-tos \
--no-eff-email
